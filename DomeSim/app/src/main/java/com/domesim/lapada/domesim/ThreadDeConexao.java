package com.domesim.lapada.domesim;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.EditText;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.net.Socket;

public class ThreadDeConexao extends Thread {
    private ArrayList<String> log = new ArrayList<String>();
    private ServerSocket servidor;
    private Socket cliente;
    private MainActivity Tprincipal;
    private EditText comandoEnviado;
    private ArrayList<String> listaPA = new ArrayList<String>();
    private ArrayList<Integer>listaPortas = new ArrayList<Integer>();
    private Handler handler;
    private GerenciadorDeConexao gerenciadorDeConexao;

    public ThreadDeConexao(MainActivity endereco)
    {
        Tprincipal = endereco;
        handler = new Handler(endereco); // esse handler servirá para enviar mensagens da thread segundária para a principal
       // gerenciadorDeConexao = ger;
    }

    public void Conecta (int porta) throws  IOException
    {
        int contPaNotConecteds=0;

        listaPA.add("192.168.43.8"); //174
        listaPA.add("192.168.43.2");
        listaPA.add("192.168.43.3");

        if(listaPA.isEmpty())
        {
            Log.i("Aviso:", "A lista de Pas está vazia!");
            handler.sendMessage(Message.obtain(handler, 0, "Não foi possível conectar com PAs!"));
            handler.sendMessage(Message.obtain(handler,0,"Não existem PAs registrados!"));
            return;
        }

        for (String pa : listaPA)
        {
            try
            {
                handler.sendMessage(Message.obtain(handler, 0, "Tentando Conectar..."));
                Log.i("Conn", "Tentando conectar....");

                Socket cliente = new Socket(pa, 12345); //aqui vem a lista de PA e Porta
                handler.sendMessage(Message.obtain(handler, 0, "Conectado com o PA: " + pa));
                Tprincipal.sockets.add(cliente); //envio de referencia de socket para lista de sockets
                handler.sendMessage(Message.obtain(handler,1,cliente));

                //gerenciadorDeConexao.addSocket(cliente);// envia a referencia do socket para ser tratato em outra classe
                log.add("O DOME conectou ao PA: " + pa);
                Log.i("conn", "Conectou com PA " + pa);
            }
            catch (UnknownHostException e)
            {
                Log.i("Aviso: Ocorreu um:","UnknownHostException ");
                contPaNotConecteds++; //adiciona um ao numero de PAS mal sucedidos
                handler.sendMessage(Message.obtain(handler, 0, "Não foi possivel se conectar com o PA: " + pa));
            }
            catch (IOException e1)
            {
                Log.i("Aviso: Ocorreu um:","IOException ");
                contPaNotConecteds++; //adiciona um ao numero de PAS mal sucedidos
                handler.sendMessage(Message.obtain(handler, 0, "Não foi possivel se conectar com o PA: " + pa));
            }
            catch (RuntimeException r1)
            {
                Log.i("Aviso: Ocorreu um:", "RuntimeException ");
            }

            if(contPaNotConecteds==listaPA.toArray().length)
            {
                handler.sendMessage(Message.obtain(handler, 0, "DOME não contectou a nenhum PA. Tente novamente ou manualmente!"));
            }

        }

      cliente.close();


    }

 @Override
     public void run()
    {
         Looper.prepare();
         try
         {
             Conecta(12345);
         }
         catch(IOException e)
         {
             handler.getMessageName(Message.obtain(handler,0,"Houve um IOException!"));
         }
         catch (RuntimeException e1)
         {
             handler.getMessageName(Message.obtain(handler, 0, "Houve um RuntimeException!"));
         }

     }
 }//fim classe
