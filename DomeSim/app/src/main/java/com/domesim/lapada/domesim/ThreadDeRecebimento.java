package com.domesim.lapada.domesim;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;


/**
 * Created by Carlos David on 22/05/2015.
 */
public class ThreadDeRecebimento extends  Thread {
    private String comando;
    private Scanner scannerDaRede;
    private Handler handler;
    private Socket socketDaRede;
    private EstadosDoDispositivo estadoAtualDispositivo = EstadosDoDispositivo.ESPERANDODISPOSITIVOS;

    public ThreadDeRecebimento(Socket socket, Handler handler) {
        this.socketDaRede = socket;
        this.handler = handler;
        try
        {
            scannerDaRede = new Scanner(socket.getInputStream());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void receber()  {
        while (true) {
            if (scannerDaRede.hasNextLine()) {
                InterpretaComando(scannerDaRede.nextLine());
            }
        }
    }

    //Método que interpretará os retornos dos PAs
    public void InterpretaComando(String comando) {
        String[] comandos = comando.split("\\.");
        // Aqui vem os comando para envio de endereçamento dos Dispositivos
        if(getEstadoDispositivo() == EstadosDoDispositivo.ESPERANDODISPOSITIVOS &&
                comandos[0].equals("00"))
        {
            Log.i("Aviso:","Estado do dispositivo: Esperando Dispositivos");
            Log.i("Aviso:","Comando Recebido:"+comando.substring(0,6));

            if(comandos[1].equals("255"))
            {
                Log.i("Aviso:", "Comando intepretado!");
                //manda o tipo do Dispositivo(PA) para a threadPrincipal gerar o Endereço
                Message.obtain(handler,3,socketDaRede).sendToTarget();
                Message.obtain(handler,2,comandos[2]).sendToTarget();
                Log.i("I/Con:","Dome recebeu a requisição de Endereçamento!");
            }

            if(comandos[1].equals("001")){
                Message.obtain(handler,0,"PA ligado.").sendToTarget();
                Log.i("I/Con:", "PA ligado.");
            }
            else if(comandos[1].equals("000"))
            {
                Message.obtain(handler,0,"PA desligado.").sendToTarget();
                Log.i("I/Con:", "PA desligado.");
            }
        }


    }

    @Override
    public void run() {
        Looper.prepare();
        receber();
    }
    public EstadosDoDispositivo getEstadoDispositivo(){
        return this.estadoAtualDispositivo;
    }
    public void setEstadoDispositivo(EstadosDoDispositivo estado){
        this.estadoAtualDispositivo = estado;
    }

}
