package com.domesim.lapada.domesim;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends Activity implements Handler.Callback
{
    public Handler handler;
    public final ArrayList<Socket> sockets = new ArrayList<Socket>();
    public EditText logdatela;
    private final String enderecoDisposito = "DD";
    private final ArrayList<String> listaEnderecosDispositivos = new ArrayList<String>();
    private int ultimoEnderecoDispositivo=0;
    private Socket socketRetorno;

	@Override
	protected void onCreate(Bundle savedInstanceState)
    {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
        logdatela =(EditText)findViewById(R.id.editText1);
        chamaThread(1); //Ao iniciar ele já tenta conectar
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
    {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);

        return true;

    }
    //ao clicar no botao Ligar
    public void AoClicar(View view)
    {

    }

    public void LigarPA(View view)throws IOException
    {
        //new ThreadDeEnvio(sockets,this).start();
        EditText texto = (EditText)findViewById(R.id.editText);
        String enderecoPA = texto.getText().toString();
        if(enderecoPA.equals("")){
           Toast.makeText(this,"Endereço inválido!",Toast.LENGTH_SHORT).show();
            //new ThreadDeEnvio(sockets,this,"000.001.000"+enderecoPA).start();
        }
        else
        {
            new ThreadDeEnvio(sockets,this,"000.001."+enderecoPA).start();
        }
        texto.setText("");

    }

    public void DesligarPA(View view)
    {

    }
    public void DetectarPA(View view)
    {
        new ThreadDeEnvio(sockets,this,"000.255.255").start();
    }

    public void chamaThread(Integer codigoDaThread)
    {
        switch(codigoDaThread)
        {
            case 1:
                ThreadDeConexao dome = new ThreadDeConexao(this);
                dome.start();
                break;
        }
    }

    public void chamaThread(Integer codigoDaThread,Socket socket)
    {
        switch (codigoDaThread)
        {

            case 2:
                //Deixa a Thread de recepção ativa
                ThreadDeRecebimento threadDeRecebimento = new ThreadDeRecebimento(socket,new Handler(this));
                threadDeRecebimento.start();
                break;
        }
    }


    @Override
    public boolean handleMessage(Message msg)
    {
        switch (msg.what)
        {
            case 0: //Para enviar textos na Activity
                Toast.makeText(this,(String)msg.obj,Toast.LENGTH_SHORT).show();
                logdatela.append((String)msg.obj+"\n");

                break;
            case 1: //inicia a thread de Recebimento
                chamaThread(2, (Socket) msg.obj);
                break;

            case 2: //Este caso se inicia para Gerar o Endereço e enviar de volta ao PA
                Log.i("Aviso:","Endereço ainda não gerado");
                String enderecoGerado = GerarEndereco((String)msg.obj);
                Log.i("Aviso:","Endereco Gerado:"+enderecoGerado);
               // ThreadDeEnvio threadDeEnvio = new ThreadDeEnvio(sockets, this,
                 //           "001.255."+enderecoGerado);
                ThreadDeEnvio threadDeEnvio = new ThreadDeEnvio(socketRetorno, this,
                                   "001.255."+enderecoGerado);
                new Thread(threadDeEnvio).start();

                Log.i("Aviso:", " Enviado ao Dispositivo seu endereço:" + enderecoGerado);
                break;
            case 3: //este caso trata o evento de endereçamento. guarda a referencia do socket que requer
                //Endereçamento para que apenas ele seja endereçado com o seu valor
                this.socketRetorno = (Socket)msg.obj;
                break;
        }
        return true;
    }

    public String getEnderecoDispositivo(){
        return  this.enderecoDisposito;
    }

    public String GerarEndereco(String tipoDispositivo)
    {
        String enderecoRetornado=null;
      //Criação de um endereço unico
        Random sorteador = new Random();
        int enderecoGerado = sorteador.nextInt(254);
        //inicializando lista para evitar bug no loop seguinte
        for (int i=0; i<=254;i++){
            listaEnderecosDispositivos.add("0");
        }
        for(int i=0; i < listaEnderecosDispositivos.toArray().length ;i++ )
        {
            if(enderecoGerado == Integer.parseInt(listaEnderecosDispositivos.get(i)))
            {
                enderecoGerado = sorteador.nextInt(254);
                continue;
            }
        }
        //verificação das casas decimais para evitar NullPointerException quando chegar ao PA
        if(Integer.toString(enderecoGerado).length()==2){
            String aux = "0"+Integer.toString(enderecoGerado);
            enderecoRetornado = aux;
            //enderecoGerado = Integer.parseInt(aux);
        }
        else if(Integer.toString(enderecoGerado).length()==1)
        {
            String aux = "00"+Integer.toString(enderecoGerado);
            enderecoRetornado = aux;
            //enderecoGerado = Integer.parseInt(aux);
        }
        else {
            enderecoRetornado = Integer.toString(enderecoGerado);
        }

        //Implementação da separaçã de Canais por dispositivo
        //--Caso de ser um PA Normal - 1 Canal
        if(tipoDispositivo.equals("000"))
        {
            listaEnderecosDispositivos.add(ultimoEnderecoDispositivo,Integer.toString(enderecoGerado));
            ultimoEnderecoDispositivo++;
            return enderecoRetornado;
           // return enderecoGerado;
        }
        //--Caso de ser um PA de 2 Canais
        else if(tipoDispositivo.equals("100"))
        {
            listaEnderecosDispositivos.add(ultimoEnderecoDispositivo,Integer.toString(enderecoGerado));
            ultimoEnderecoDispositivo = ultimoEnderecoDispositivo+2;
            return enderecoRetornado;
            //return enderecoGerado;
        }
        //--Caso de ser um PA RGB - 4 Canais
        else if(tipoDispositivo.equals("010"))
        {
            listaEnderecosDispositivos.add(ultimoEnderecoDispositivo,Integer.toString(enderecoGerado));
            ultimoEnderecoDispositivo = ultimoEnderecoDispositivo+4;
            return enderecoRetornado;
            //return Integer.toString(enderecoGerado);
        }
        else
        { return ("0");}
    }


}
