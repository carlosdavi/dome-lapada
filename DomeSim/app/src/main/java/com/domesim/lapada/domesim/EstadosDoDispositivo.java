package com.domesim.lapada.domesim;

/**
 * Created by davi on 05/07/15.
 */
public enum EstadosDoDispositivo {
    ESPERANDODISPOSITIVOS,
    EMOPERACAO
}
