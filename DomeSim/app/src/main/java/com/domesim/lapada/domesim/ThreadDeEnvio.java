package com.domesim.lapada.domesim;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by Carlos David on 22/05/2015.
 */
public class ThreadDeEnvio extends Thread
{
    private String comando;
    private ThreadDeConexao threadDeConexao;
    private ArrayList<Socket> listaSockets = new ArrayList<Socket>();
    public Handler handler;
    private String mensagem;
    private Socket socket=null;

    public ThreadDeEnvio(ArrayList<Socket> lista, MainActivity Tprincipal)
    {
        this.listaSockets = lista; //referencia da thread de conexao que contem os sockets
        this.handler = new Handler(Tprincipal);
    }
    public ThreadDeEnvio(ArrayList<Socket> lista, MainActivity Tprincipal,String mensagem)
    {
        this.listaSockets = lista; //referencia da thread de conexao que contem os sockets
        this.handler = new Handler(Tprincipal);
        this.mensagem = mensagem;
    }
    public  ThreadDeEnvio(Socket socket, MainActivity Tprincipal,String mensagem)
    {
        this.socket = socket;
        this.handler = new Handler(Tprincipal);
        this.mensagem = mensagem;
    }

    public void enviar() throws IOException
    {
        DataOutputStream comando=null;
        //Não precisa fechar o OutputStream, caso contrário ocasionará bug
        for(Socket pa: listaSockets)
        {

            comando = new DataOutputStream(pa.getOutputStream());
            if(this.mensagem.isEmpty())
            {
                comando.writeBytes("1.1.1\n");
                handler.sendMessage(Message.obtain(handler, 0, "Comando enviado: 1.1.1"));
                Log.i("Con/I:", "Comando enviado: 1.1.1");
            }
            else
            {
                comando.writeBytes(this.mensagem+"\n");
                handler.sendMessage(Message.obtain(handler, 0, "Comando enviado:" + this.mensagem));
                Log.i("Con/I:", "Comando enviado:"+this.mensagem);
                comando.writeBytes(" \n");
            }

        }

    }
    //Sobrecarga de método
    public boolean enviar(String x) throws IOException
    {
        DataOutputStream comando=null;
        //Não precisa fechar o OutputStream, caso contrário ocasionará bug
        comando = new DataOutputStream(socket.getOutputStream());
            if(this.mensagem.isEmpty())
            {
                comando.writeBytes("1.1.1\n");
                handler.sendMessage(Message.obtain(handler, 0, "Comando enviado: 1.1.1"));
                Log.i("Con/I:", "Comando enviado: 1.1.1");
            }
            else
            {
                comando.writeBytes(this.mensagem+"\n");
                handler.sendMessage(Message.obtain(handler, 0, "Comando enviado:" + this.mensagem));
                Log.i("Con/I:", "Comando enviado:"+this.mensagem);
                comando.writeBytes(" \n");
            }
    return (true);
        }

    //}

    @Override
    public void run(){

        try
        {
            //Sera que falta um Looper.prepare(); ?
            if(socket != null)
            {
                enviar("");
            }
            else
            {
                enviar();
            }
        }
        catch (IOException e)
        {
            handler.sendMessage(Message.obtain(handler, 0, "Houve um IOException"));
        }

    }

}
